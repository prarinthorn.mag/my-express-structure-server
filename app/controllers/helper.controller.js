const { validationResult } = require("express-validator");
const message = require("../libs/message.lib");
const logger = require("../libs/logger.lib");
const sharp = require("sharp");
const path = require("path");
const config = require("../../config/config");
const app_root_path = require("app-root-path");
const file_path = app_root_path.resolve(config.file_path);
const sizeOf = require("buffer-image-size");
const fs = require("fs");
// const db = require("../models");
// const {
//   sequelize, File
// } = db;

exports.get_yesterday = () => {
  const d = new Date();
  return `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate() - 1}`;
};

exports.check_error = function (req, res, next) {
  try {
    const errors = validationResult(req);
    if (errors.array().length > 0) {
      let messages = message.get_message_validates(errors);
      messages = messages.unique();
      return res.handle_response(false, { messages, code: "001" });
    }
  } catch (e) {
    logger.error(e);
    return res.handle_response(false, { code: "950" });
  }

  return next();
};

exports.convert_array_to_object = function (array, key = "id") {
  return array.reduce(function (acc, cur, i) {
    acc[cur[key]] = cur;
    return acc;
  }, {});
};

exports.resize = async function (file, options = {}) {
  let { width_size } = options;
  width_size = width_size || 1000;
  let dimensions = sizeOf(file.data);
  if (dimensions.width > width_size) {
    let data = await sharp(file.data).resize(width_size).toBuffer();
    file.data = data;
  }

  return file;
};

exports.get_extension_file = function (filename, replace = ".") {
  return path.extname(filename).replace(replace, "");
};

exports.get_file_name_stored = function (filename) {
  let date_now = Date.now();
  let file_name_stored = `${date_now}${this.get_extension_file(filename, "")}`;
  return file_name_stored;
};

// exports.save_file = async function (file, user, folder) {
//   let user_id = user ? user.id : null;
//   let file_name_stored = this.get_file_name_stored(file.name);
//   let _file_path = `${file_path}${folder}/`;
//   let _file = {
//     file_name: file.name,
//     file_name_stored,
//     file_path: _file_path,
//     file_extension: this.get_extension_file(file.name),
//     file_size: file.size,
//     created_by: user_id
//   };

//   let result = await File.create(_file);
//   if (!fs.existsSync(_file_path)){
//     fs.mkdirSync(_file_path);
//   }

//   fs.writeFile(`${_file_path}/${file_name_stored}`, file.data,  "binary", function(err) { });

//   return result;
// }
