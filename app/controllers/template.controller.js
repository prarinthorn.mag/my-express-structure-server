const db = require("../models");
const { QueryTypes } = require("sequelize");
const { sequelize, Sequelize, Expense } = db;
const template_ = require("../query/template.query");

const { logger, message } = require("../libs");
const { customLog } = logger;

var cron = require('node-cron');

// JOB schedule ตั้งการทำงานซ้ำตามช่วงเวลา
cron.schedule('0 0 9 * * *', async function () {

    // FUNCTION....

});

exports.template_controller_js = async function (req, res, next) {
    customLog("template_controller_js", "notify");
    try {

        // -------------------------------- SELECT -----------------------------------------
        const sql = template_.template()
        const records = await sequelize.query(sql, { type: QueryTypes.SELECT });


        // -------------------------------- INSERT -----------------------------------------
        const transaction_insert = await sequelize.transaction();
        try {
            const expense = Expense.build(Object.assign(req.body,
                {
                    short_log: 'Create Ticket',
                    create_by: user,
                    update_by: user,
                    contract_id: 'undefined',
                    technician: "undefined",
                    update_date: await getCurrentDate()
                }));
            await expense.save({ transaction_insert });
            await transaction_insert.commit();
        }
        catch (error) {
            logger.error(error);
            await transaction_insert.rollback();
        }

        // -------------------------------- UPDATE -----------------------------------------

        const transaction_update = await sequelize.transaction();
        try {
            // const ticket_display = db.ticket_display.build();

            const UPDATE_VALUE = Object.assign(req.body,
                {
                    // short_log: 'Update',
                    // update_by: user,
                    update_date: await getCurrentDate()
                })

            await Expense.update(UPDATE_VALUE, { where: { ticket_id: ticket_id }, transaction_update });
            await transaction_update.commit();

        } catch (error) {
            logger.error(error);
            await transaction_update.rollback();
        }

        // -------------------------------- DELETE -----------------------------------------

        const sql_delete = `DELETE from ticket_action_plan where action_plan_id = '${action_plan_id}' ;`
        // const records_delete = await sequelize.query(sql_delete);


        return res.json({
            success: true,
            data: records,
        });
    } catch (e) {
        logger.error(e)
    }
    return res.handle_response(false, {
        message: message.get_message("something_went_wrong"),
        code: "999",
    });
};



