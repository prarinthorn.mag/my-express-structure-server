const Big = require('big.js');

const mul = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.mul(b).toFixed();
  return Number(result);
};

const div = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.div(b).toFixed();
  return Number(result);
};

const plus = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.plus(b).toFixed();
  return Number(result);
};

const minus = (a, b) => {
  a = Number(a);
  b = Number(b);

  let x = new Big(a);
  let result = x.minus(b).toFixed();
  return Number(result);
};

const count_decimals = (value) => {
  let text = value.toString()
  if (text.indexOf('e-') > -1) {
    let [base, trail] = text.split('e-');
    let deg = parseInt(trail, 10);
    return deg;
  }

  if (Math.floor(value) !== value) {
    return value.toString().split(".")[1].length || 0;
  }

  return 0;
}

const fee = (fee_type, fee, amount) => {
  fee = new Big(fee);
  amount = new Big(amount);
  let net_fee;
  if (fee_type === 'P') {
    net_fee = (amount.mul(fee)).div(100);
  } else {
    net_fee = fee;
  }

  return net_fee.toFixed(2);
}

const to_fixed = (a, digits = 2) => {
  let x = new Big(a);
  return x.toFixed(digits);
}

module.exports = {
  mul,
  div,
  plus,
  minus,
  count_decimals,
  fee,
  to_fixed
}
