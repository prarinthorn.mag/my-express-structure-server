const { logger } = require("../libs/logger.lib");
const config = require("../../config/config");
const { stringify } = require("querystring");
const axiosRetry = require("axios-retry");
var FormData = require("form-data");
var request = require("request");
const https = require("https");
let axios = require("axios");

// const FormData = require('form-data');
// const qs = require('qs');

axios.defaults.timeout = 120000;
axiosRetry(axios, {
  retries: 5,
  retryDelay: (retryCount) => {
    return retryCount * 2000;
  },
});
const httpsAgent = new https.Agent({ rejectUnauthorized: false });
const cip_rop_service_auth = {
  username: config.cip_rop_service.auth.username,
  password: config.cip_rop_service.auth.password,
};

const cip_rop_service_header = {
  "Content-Type": "application/json; charset=utf-8",
};

const cip_rop_service_get = async (path, options = {}) => {
  const { params } = options;
  const _params = params || {};

  const config_axios = {
    params: _params,
    auth: cip_rop_service_auth,
    headers: cip_rop_service_header,
    httpsAgent: httpsAgent,
  };

  const result = await axios.get(
    // `${config.cip_rop_service.domain}${path}`,
    `${path}`,
    config_axios
  );
  return result;
};

const cip_rop_service_post = async (path, body, options = {}) => {
  const { content_type, arraybuffer } = options;
  let _body = body || {};

  const config_axios = {
    // auth: cip_rop_service_auth,
    auth: {
      username: "rss",
      password: "ui7Rt84E5E7udb",
    },
    headers: cip_rop_service_header,
    httpsAgent: httpsAgent,
  };
  if (arraybuffer === true) {
    config_axios.responseType = "arraybuffer";
  }

  if (content_type) {
    config_axios.headers["Content-Type"] = content_type;
    if (content_type === "application/x-www-form-urlencoded") {
      _body = stringify(body);
    }
  }

  const result = await axios.post(
    // `${config.cip_rop_service.domain}${path}`,
    `${path}`,
    _body,
    config_axios
  );
  return result;
};

const cip_rop_service_posts = async (path, body, options = {}) => {
  const { content_type, arraybuffer } = options;
  let _body = body || {};

  const config_axios = {
    auth: {
      username: "rop",
      password: "iu7ej8R08DDo33",
    },
    headers: cip_rop_service_header,
    httpsAgent: httpsAgent,
    responseType: "application/json",
  };
  // if (arraybuffer === true) {
  //   config_axios.responseType = "application/json";
  // }

  if (content_type) {
    config_axios.headers["Content-Type"] = content_type;
    if (content_type === "application/x-www-form-urlencoded") {
      _body = stringify(body);
    }
  }

  const result = await axios.post(
    // `${config.cip_rop_service.domain}${path}`,
    `${path}`,
    _body,
    config_axios
  );
  return result;
};

const line_message = async (message) => {
  request(
    {
      method: "POST",
      uri: "https://notify-api.line.me/api/notify",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      auth: {
        bearer: "UppyApFqvAMaYn8yFmqosLFdeS7hdYfeHJ3sJCitJZw",
      },
      form: {
        message: message,
      },
    },
    (err, body) => {
      if (err) {
        return { success: false, data: [] };
      } else {
        return { success: true, data: body };
      }
    }
  );
};

const line_message_private = async (message, stickerPkg, stickerId) => {
  request(
    {
      method: "POST",
      uri: "https://notify-api.line.me/api/notify",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      auth: {
        bearer: "UppyApFqvAMaYn8yFmqosLFdeS7hdYfeHJ3sJCitJZw", //dmtK5DXETousMlL0dKqipnm3Cq5TFDd7RXkcF3n81pA
      },
      form: {
        message: message,
        stickerPackageId: stickerPkg,
        stickerId: stickerId,
      },
    },
    (err, body) => {
      if (err) {
        return { success: false, data: [] };
      } else {
        return { success: true, data: body };
      }
    }
  );
};

const authen = async (path, body, options = {}) => {
  const { content_type, arraybuffer } = options;
  let _body = body || {};

  const config_axios = {
    // auth: {
    //   username: "rss_intf_dev",
    //   password: "xb7VmEJbZsveC2bB",
    // },
    auth: {
      username: "rss_intf",
      password: "2C7L9hZnKVNARu2J",
    },
    headers: cip_rop_service_header,
    httpsAgent: httpsAgent,
    responseType: "application/json",
  };

  if (content_type) {
    config_axios.headers["Content-Type"] = content_type;
    if (content_type === "application/x-www-form-urlencoded") {
      _body = stringify(body);
    }
  }

  const result = await axios.post(`${path}`, _body, config_axios);
  // console.log('authen' , result.data)
  return result.data;
};

module.exports = {
  cip_rop_service: {
    line: line_message,
    get: cip_rop_service_get,
    post: cip_rop_service_post,
    linep: line_message_private,
    posts: cip_rop_service_posts,
    authen: authen,
  },
};
