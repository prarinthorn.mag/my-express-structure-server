const stackTrace = require('stack-trace');
const dateFormat = require('dateformat');

save_log = (user, _event, _function) => {
  const { dwh_log } = require("../models");
  let params = {
    'event': _event,
    event_by: user.user_id,
    'function': _function
  };

  dwh_log.save(params);
}

const info = async (message) => {
  let date = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
  console.log(`[${date}] info: `, message);
}

const error = async (e) => {
  let date = dateFormat(new Date(), "yyyy-mm-dd HH:MM:ss");
  let trace = stackTrace.get();
  console.error(`[${date}] error: ${trace[1].getFileName()}:${trace[1].getLineNumber()}`);
  console.error(e)
}

const customLog = async (text, key = "notify") => {
  const color = {
    error: "\x1b[31m",
    success: "\x1b[32m",
    warning: "\x1b[33m",
    notify: "\x1b[34m",
    magenta: "\x1b[35m",
    cyan: "\x1b[36m",
    white: "\x1b[37m",
  };
  await console.log(`${color[key]}%s${color[key]}\x1b[37m`, `${text}`);
}

const elastic = async (params = {}) => {
  params.datetime = new Date();
  console.log(JSON.stringify(params));
}

module.exports = {
  info,
  error,
  elastic,
  customLog
}
