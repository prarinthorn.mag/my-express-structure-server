exports.get_message_validates = (errors) => {
  let messages = errors.array().map((error) => {
    let { key, params } = error.msg;
    params = JSON.parse(JSON.stringify(params || {}));
    let { field } = params;
    if (field) {
      field = __(field);
      params.field = field;
    }

    try {
      return __(key, params).toLowerCase().capitalize();
    } catch (e) {
      return '';
    }
  });

  return messages;
}

exports.get_message = (key, params) => {
  params = JSON.parse(JSON.stringify(params || {}));
  let { field } = params;
  if (field) {
    field = __(field);
    params.field = field;
  }

  return __(key, params).toLowerCase().capitalize();
}
