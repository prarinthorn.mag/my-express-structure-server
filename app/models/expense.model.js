module.exports = (sequelize, Sequelize) => {
    const Model = sequelize.define(
        "expense", {
        invoice_no: {
            type: Sequelize.STRING,
            primaryKey: true,
        },
        runing_code: {
            type: Sequelize.DECIMAL,
        },
        vendor_id: {
            type: Sequelize.STRING,
        },
        actual_vendor_name: {
            type: Sequelize.STRING,
        },
        actual_vendor_site: {
            type: Sequelize.STRING,
        },
        bank_account: {
            type: Sequelize.STRING,
        },
        amount: {
            type: Sequelize.FLOAT(8, 2),
        },
        invoice_total: {
            type: Sequelize.FLOAT(8, 2),
        },
        invoice_type: {
            type: Sequelize.STRING,
        },
        pre_payment_number: {
            type: Sequelize.STRING,
        },
        description: {
            type: Sequelize.TEXT,
        },
        invoice_reference: {
            type: Sequelize.STRING,
        },
        status: {
            type: Sequelize.STRING,
        },
        approver: {
            type: Sequelize.STRING,
        },
        finished_date: {
            type: Sequelize.DATE,
        },
        due_date: {
            type: Sequelize.DATE,
        },
        create_by: {
            type: Sequelize.STRING,
        },
        create_date: {
            type: Sequelize.DATE,
        },
        update_date: {
            type: Sequelize.DATE,
        },
        cost_center: {
            type: Sequelize.STRING,
        },
        ReferenceObjectNodeSenderTechnicalID: {
            type: Sequelize.STRING,
        },
        ChangeStateID: {
            type: Sequelize.STRING,
        },
        BusinessTransactionDocumentID: {
            type: Sequelize.STRING,
        },
        UUID: {
            type: Sequelize.STRING,
        },
        Note: {
            type: Sequelize.STRING,
        },
        Status_byDesign: {
            type: Sequelize.STRING,
        },
        ObjectID: {
            type: Sequelize.STRING,
        },
        ID__: {
            type: Sequelize.STRING,
        },
        SupplierInvoiceLifeCycleStatusCode: {
            type: Sequelize.STRING,
        },
        ApprovalStatusCode: {
            type: Sequelize.STRING,
        },
        PostingStatusCode: {
            type: Sequelize.STRING,
        }
    }, {
        freezeTableName: true,
        timestamps: false,
    }
    );

    return Model;
};