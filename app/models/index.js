const Sequelize = require("sequelize");
const { DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD, DATABSE_HOST, DATABASE_DIALECT } = process.env

const sequelize = new Sequelize(DATABASE_NAME, DATABASE_USERNAME, DATABASE_PASSWORD,
    {
        host: DATABSE_HOST,
        dialect: DATABASE_DIALECT,
        dialectOptions: { connectTimeout: 10000 },
        pool: {
            max: 5,
            min: 0,
            idle: 10000,
            acquire: 20000
        }
    }
);

Sequelize.postgres.DECIMAL.parse = function (value) {
    return Number(value);
};

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

let role = require("./role.model.js")(sequelize, Sequelize);
let product = require("./product.model.js")(sequelize, Sequelize);
let partner = require("./partner.model.js")(sequelize, Sequelize);
let activity = require("./activity.model.js")(sequelize, Sequelize);
let user_logon = require("./user_logon.model.js")(sequelize, Sequelize);
let sap_module = require("./sap_module.model.js")(sequelize, Sequelize);
let technicain = require("./technicain.model.js")(sequelize, Sequelize);
let department = require("./department.model.js")(sequelize, Sequelize);
let problem_type = require("./problem_type.model.js")(sequelize, Sequelize);
let sub_activity = require("./sub_activity.model.js")(sequelize, Sequelize);
let product_group = require("./product_group.model.js")(sequelize, Sequelize);
let request_status = require("./request_status.model.js")(sequelize, Sequelize);
let cloud_provider = require("./cloud_provider.model.js")(sequelize, Sequelize);
let user_assignment = require("./user_assignment.model.js")(sequelize, Sequelize);

let address = require("./address.model.js")(sequelize, Sequelize);
let customer = require("./customer.model.js")(sequelize, Sequelize);
let customer_info = require("./customer_info.model.js")(sequelize, Sequelize);
let customer_server = require("./customer_server.model.js")(sequelize, Sequelize);
let customer_sap_user = require("./customer_sap_user.model.js")(sequelize, Sequelize);


let contract = require("./contract.model.js")(sequelize, Sequelize);
let attachments = require("./attachments.model.js")(sequelize, Sequelize);


db.role = role;
db.product = product;
db.partner = partner;
db.activity = activity;
db.user_logon = user_logon;
db.sap_module = sap_module;
db.technicain = technicain;
db.department = department;
db.problem_type = problem_type;
db.sub_activity = sub_activity;
db.product_group = product_group;
db.request_status = request_status;
db.cloud_provider = cloud_provider;
db.user_assignment = user_assignment;

db.address = address;
db.customer = customer;
db.customer_info = customer_info;
db.customer_server = customer_server;
db.customer_sap_user = customer_sap_user;

db.contract = contract;
db.attachments = attachments;

module.exports = db;