
const template = require("../controllers/template.controller")
const api_v1 = "/api/v1/master";

module.exports = function (app) {

    app.route(`${api_v1}/resetPassword`).post(template.template_controller_js);

}