module.exports = {
  env: "development",
  postgres: {
    user: process.env.POSTGRES_USER,
    host: process.env.POSTGRES_HOST,
    database: process.env.POSTGRES_DATABASE,
    password: process.env.POSTGRES_PASSWORD,
    port: process.env.POSTGRES_PORT,
  },
  log_path: "./logs/",
  debug: process.env.DEBUG || "true",
  secret: "vsyqki3s2opf962kcsdr",
  expire: process.env.EXPIRE || "30m",
  allowed_origins:
    process.env.ALLOWED_ORIGINS,
  file_path: process.env.FILE_PATH || "./files/",
  cip_rop_service: {
    domain:
      process.env.CIP_ROP_DOMAIN ||
      "https:/_________________________",
    auth: {
      username: process.env.CIP_ROP_USERNAME,
      password: process.env.CIP_ROP_PASSWORD,
    },
  },
};