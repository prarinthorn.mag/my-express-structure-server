const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const moment = require("moment-timezone");
const i18n = require("i18n");
const cors = require("cors");

const config = require("./config");

i18n.configure({
    locales: ["en", "th"],
    directory: __dirname + "/locales",
    register: global,
    updateFiles: false,
});

const db = require("../app/models");
require("./log");

function handle_response(res, success, options = {}) {
    if (success) {
        let { data, message } = options;
        let response = {};
        if (message) {
            response.message = message;
        }

        if (data) {
            response.data = data;
        }

        return res.json({
            success: true,
            response,
        });
    } else {
        let { code, message, messages, message_params } = options;
        return res.json({
            success: false,
            response: {
                error_code: code,
                error_message: message,
                error_messages: messages,
            },
        });
    }
}

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

Array.prototype.unique = function () {
    let a = [];
    for (let i = 0, l = this.length; i < l; i++)
        if (a.indexOf(this[i]) === -1) a.push(this[i]);

    return a;
};

Array.prototype.clean = function (deleteValue) {
    for (let i = 0; i < this.length; i++) {
        if (this[i] === deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }

    return this;
};

String.prototype.replaceAll = function (search, replacement) {
    let target = this;
    return target.replace(new RegExp(search, "g"), replacement);
};

module.exports = function () {
    let app = express();
    let allowedOrigins = config.allowed_origins.split(",");
    console.log("allowedOrigins", allowedOrigins);
    let corsOptions = {
        origin: function (origin, callback) {
            if (!origin) return callback(null, true);
            if (allowedOrigins.indexOf(origin) === -1) {
                let msg =
                    "The CORS policy for this site does not " +
                    "allow access from the specified Origin.";
                return callback(new Error(msg), false);
            }

            return callback(null, true);
        },
    };

    app.use(cors(corsOptions));

    app.use(fileUpload({
        createParentPath: true,
    }));

    // app.use(express.static("files"));

    morgan.token("date", (req, res, tz) => {
        return moment().tz(tz).format();
    });

    morgan.format(
        "myformat",
        '[:date[Asia/Bangkok]] ":method :url" :status :res[content-length] - :response-time ms'
    );
    app.use(morgan("myformat"));
    app.use(i18n.init);

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(function (req, res, next) {
        res.handle_response = handle_response.bind(null, res);

        i18n.setLocale(req, req.headers.lang);

        next();
    });

    ////-------------------------Open only first install --------------------------------
    // if (process.env.NODE_ENV === "development") {
    //     db.sequelize.sync({});
    //     db.sequelize.sync({ alter: true });
    // }
    ////-----------------------After first run please close------------------------------

    app.use(function (req, res, next) {
        var ip = req.headers['x-forwarded-for'] || req.socket.remoteAddress
        console.log('authorization :: ', req.headers.authorization);
        console.log('ip :: ', ip);
        next();
    });

    require("../app/routes/template.routes")(app);

    app.use(function (req, res, next) {
        if (req.method != "OPTIONS") {
            res.status(404).send({ error: "Not found" });
        } else {
            next();
        }
    });

    return app;
};