/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('dwh_role', {
    role_id: {
      type: 'varchar(3)',
      primaryKey: true,
      notNull: true,
      comment: 'รหัส Role'
    },
    role_name: {
      type: 'varchar(500)',
      comment: 'ชื่อ'
    },
    role_desc: {
      type: 'varchar(500)',
      comment: 'รายละเอียด'
    }
  });
};

exports.down = (pgm) => {

};
