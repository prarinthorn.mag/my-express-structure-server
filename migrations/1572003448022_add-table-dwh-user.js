/* eslint-disable camelcase */

exports.shorthands = undefined;

exports.up = (pgm) => {
  pgm.createTable('dwh_user', {
    user_id: {
      type: 'varchar(3)',
      primaryKey: true,
      notNull: true,
      comment: 'รหัส USER'
    },
    user_id_card: {
      type: 'varchar(20)',
      comment: 'รหัสบัตรประชาชน'
    },
    user_password: {
      type: 'varchar(500)'
    },
    user_status: {
      type: 'varchar(1)',
      comment: 'สถานะ (Y=Active, N=Inactive)'
    },
    role_id: {
      type: 'varchar(3)',
      comment: 'รหัส Role'
    },
    update_pid: {
      type: 'varchar(13)',
      comment: 'รหัสผู้แก้ไข'
    },
    update_date: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
      comment: 'วันที่แก้ไข'
    },
    create_pid: {
      type: 'varchar(13)',
      comment: 'รหัสผู้สร้างรายการ'
    },
    create_date: {
      type: 'timestamp',
      notNull: true,
      default: pgm.func('current_timestamp'),
      comment: 'วันที่สร้างรายการ'
    }
  });
};

exports.down = (pgm) => {

};
