var https = require('https');
var fs = require('fs');

if (!global._babelPolyfill) {
  require('babel-polyfill');
}

process.env.NODE_ENV = process.env.NODE_ENV || "development";
process.env.TZ = "Asia/Bangkok";

if (process.env.NODE_ENV === "development") {
  require("dotenv").config();
}

const express = require("./config/express");
const port = process.env.PORT;
const app = express();

let server = {}
if (process.env.TYPE == "PRD") {

  const privateKey = fs.readFileSync("./cert/________________.pem");
  const certificate = fs.readFileSync("./cert/________________.pem");

  server = https.createServer({
    key: privateKey,
    cert: certificate
  }, app).listen(port);

} else {

  server = app.listen(port);

}

console.log("Express server listening on port " + port);

module.exports = server;

// const server_http = app.listen(port);
// const server = https.createServer({
//   key: privateKey,
//   cert: certificate
// }, app).listen(port);